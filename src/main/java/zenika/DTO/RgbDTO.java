package zenika.DTO;

public class RgbDTO {

    private final int red;
    private final int green;
    private final int blue;

    public RgbDTO(int r, int g, int b){
        this.red = r;
        this.green = g;
        this.blue = b;
    }

    public int getRed() {
        return red;
    }

    public int getGreen() {
        return green;
    }

    public int getBlue() {
        return blue;
    }
}
