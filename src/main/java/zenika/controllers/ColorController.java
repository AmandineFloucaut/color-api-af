package zenika.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import zenika.DTO.RgbDTO;

import java.awt.*;
import java.util.Random;

@RestController
@RequestMapping("/api")
public class ColorController {

    @Autowired
    private Random myRandom;

    @GetMapping("/colors/random")
    public ResponseEntity<RgbDTO> getRandomColor(){
        int r = this.myRandom.nextInt(256);
        int g = this.myRandom.nextInt(256);
        int b = this.myRandom.nextInt(256);
        RgbDTO randomColor = new RgbDTO(r, g, b);

        return ResponseEntity.ok(randomColor);
    }
}
